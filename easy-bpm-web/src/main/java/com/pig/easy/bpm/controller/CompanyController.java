package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.CompanyReqDTO;
import com.pig.easy.bpm.dto.response.CompanyDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.CompanyService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.CompanyVO;
import com.pig.easy.bpm.vo.request.TreeQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 公司表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
@RestController
@RequestMapping("/company")
@Api(tags = "公司管理", value = "公司管理")
public class CompanyController extends BaseController {

    @Reference
    CompanyService companyService;

    @ApiOperation(value = "查询公司列表", notes = "查询公司列表", produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@ApiParam(name = "传入公司详细信息", value = "传入json格式", required = true) @Valid @RequestBody CompanyVO companyVO) {

        if(companyVO == null
                || StringUtils.isEmpty(companyVO.getTenantId())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        CompanyReqDTO companyReqDTO = switchToDTO(companyVO, CompanyReqDTO.class);
        Result<PageInfo> result = companyService.getListByCondition(companyReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增公司", notes = "新增公司", produces = "application/json")
    @PostMapping("/insert")
    public JsonResult insert(@Valid @RequestBody CompanyVO companyVO) {

        if(companyVO == null
                || StringUtils.isEmpty(companyVO.getTenantId())
                || StringUtils.isEmpty(companyVO.getCompanyCode())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        CompanyReqDTO companyReqDTO = switchToDTO(companyVO, CompanyReqDTO.class);
        companyReqDTO.setOperatorId(currentUserInfo().getUserId());
        companyReqDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = companyService.insert(companyReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "修改公司", notes = "修改公司", produces = "application/json")
    @PostMapping("/update")
    public JsonResult updateProcess(@Valid @RequestBody CompanyVO companyVO) {

        if(companyVO == null
                || StringUtils.isEmpty(companyVO.getTenantId())
                || StringUtils.isEmpty(companyVO.getCompanyCode())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        CompanyReqDTO companyReqDTO = switchToDTO(companyVO, CompanyReqDTO.class);

        Result<Integer> result = companyService.updateByCompanyCode(companyReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除公司", notes = "删除公司", produces = "application/json")
    @PostMapping("/delete")
    public JsonResult delete(@Valid @RequestBody CompanyVO companyVO) {

        if(companyVO == null
                || StringUtils.isEmpty(companyVO.getTenantId())
                || StringUtils.isEmpty(companyVO.getCompanyCode())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        CompanyReqDTO companyReqDTO = switchToDTO(companyVO, CompanyReqDTO.class);

        companyReqDTO.setOperatorId(currentUserInfo().getUserId());
        companyReqDTO.setOperatorName(currentUserInfo().getRealName());
        companyReqDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = companyService.updateByCompanyCode(companyReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "根据公司编号获取公司详细信息", notes = "获取公司", produces = "application/json")
    @PostMapping("/getCompanyById/{companyId}")
    public JsonResult getCompanyById(@ApiParam(required = true, name = "公司编号", value = "companyId", example = "1") @PathVariable("companyId") Long companyId) {

        Result<CompanyDTO> result = companyService.getCompanyById(companyId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "根据公司code获取公司详细信息", notes = "获取公司", produces = "application/json")
    @PostMapping("/getCompanyByCode/{companyCode}")
    public JsonResult getCompanyByCode(@ApiParam(required = true, name = "公司编码", value = "companyCode", example = "pig:companyCode") @PathVariable("companyCode") String companyCode) {

        Result<CompanyDTO> result = companyService.getCompanyByCode(companyCode);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "获取公司ID name 字典", notes = "获取公司字典", produces = "application/json")
    @PostMapping("/getCompanyIdAndNameDictList")
    public JsonResult getCompanyIdAndNameDictList(@Valid @RequestBody CompanyVO companyVO) {

        if(companyVO == null){
            companyVO = new CompanyVO();
        }
        CompanyReqDTO companyReqDTO = BeanUtils.switchToDTO(companyVO,CompanyReqDTO.class);
        Result<List<ItemDTO>> result = companyService.getCompanyIdAndNameDictList(companyReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "获取公司树", notes = "获取公司树", produces = "application/json")
    @PostMapping("/getCompanyTree")
    public JsonResult getCompanyTree(@Valid @RequestBody TreeQueryVO treeQueryVO) {

        Result<List<TreeDTO>> result = companyService.getCompanyTree(treeQueryVO.getParentId(), treeQueryVO.getTenantId());
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }
}

