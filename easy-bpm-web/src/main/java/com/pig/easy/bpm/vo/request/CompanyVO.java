package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/6 21:19
 */
@Data
@ToString
public class CompanyVO implements Serializable {

    private static final long serialVersionUID = 8119550820285834266L;

    private Long companyId;

    private String companyCode;

    private Long companyParentId;

    private String companyParentCode;

    private String companyName;

    private String companyAbbr;

    private Integer companyLevel;

    private Integer companyOrder;

    private String companyIcon;

    private String companyUrl;

    private String tenantId;

    private Integer companyStatus;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private Integer pageIndex;

    private Integer pageSize;
}
