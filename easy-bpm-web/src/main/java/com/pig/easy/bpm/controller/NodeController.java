package com.pig.easy.bpm.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.pig.easy.bpm.controller.BaseController;

/**
 * <p>
 * 流程节点表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-06-19
 */
@RestController
@RequestMapping("/nodeDO")
public class NodeController extends BaseController {

}

