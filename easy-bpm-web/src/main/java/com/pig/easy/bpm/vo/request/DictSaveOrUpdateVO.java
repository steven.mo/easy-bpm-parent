package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/26 18:47
 */
@Data
@ToString
public class DictSaveOrUpdateVO implements Serializable {

    private static final long serialVersionUID = 2716585886618834546L;

    private Long dictId;

    private String dictCode;

    private String dictName;

    private String tenantId;

    private String remark;

    private Integer validState;
}
