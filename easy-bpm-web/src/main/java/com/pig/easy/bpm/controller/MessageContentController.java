package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.MessageContentQueryDTO;
import com.pig.easy.bpm.dto.request.MessageContentSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MessageContentDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.MessageContentService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.MessageContentQueryVO;
import com.pig.easy.bpm.vo.request.MessageContentSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * <p>
 * 通知内容表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
@RestController
@Api(tags = "通知内容表管理", value = "通知内容表管理")
@RequestMapping("/messageContent")
public class MessageContentController extends BaseController {

        @Reference
        MessageContentService service;

        @ApiOperation(value = "查询通知内容表列表", notes = "查询通知内容表列表", produces = "application/json")
        @PostMapping("/getList")
        public JsonResult getList(@Valid @RequestBody MessageContentQueryVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MessageContentQueryDTO queryDTO = switchToDTO(param, MessageContentQueryDTO.class);

        Result<PageInfo<MessageContentDTO>> result = service.getListByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "新增通知内容表", notes = "新增通知内容表", produces = "application/json")
        @PostMapping("/insert")
        public JsonResult insertMessageContent(@Valid @RequestBody MessageContentSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
       MessageContentSaveOrUpdateDTO saveDTO = switchToDTO(param, MessageContentSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId().intValue());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.insertMessageContent(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "修改通知内容表", notes = "修改通知内容表", produces = "application/json")
        @PostMapping("/update")
        public JsonResult updateMessageContent(@Valid @RequestBody MessageContentSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
         MessageContentSaveOrUpdateDTO saveDTO = switchToDTO(param, MessageContentSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId().intValue());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.updateMessageContent(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "删除通知内容表", notes = "删除通知内容表", produces = "application/json")
        @PostMapping("/delete")
        public JsonResult delete(@Valid @RequestBody MessageContentSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MessageContentSaveOrUpdateDTO saveDTO = switchToDTO(param, MessageContentSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId().intValue());
        saveDTO.setOperatorName(currentUserInfo().getRealName());
        saveDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = service.deleteMessageContent(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

}

