package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.pig.easy.bpm.dto.request.HistorySaveDTO;
import com.pig.easy.bpm.dto.response.HistoryDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.HistoryService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.HistoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 审批历史表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-07-01
 */
@RestController
@RequestMapping("/historyDO")
@Api(tags = "审批历史管理", value = "审批历史管理")
public class HistoryController extends BaseController {

    @Reference
    HistoryService historyService;

    @ApiOperation(value = "根据申请编号获取审批历史记录", notes = "根据申请编号获取审批历史记录", produces = "application/json")
    @PostMapping("/getHistoryByApplyId/{applyId}")
    public JsonResult getHistoryByApplyId(@ApiParam(required = true, name = "申请编号", value = "applyId", example = "1") @PathVariable("applyId") Long applyId) {

        Result<List<HistoryDTO>> result = historyService.getListByApplyId(applyId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "根据任务编号获取审批记录", notes = "根据任务编号获取审批记录", produces = "application/json")
    @PostMapping("/getHistoryByTaskId/{applyId}")
    public JsonResult getHistoryByTaskId(@ApiParam(required = true, name = "任务编号", value = "taskId", example = "1") @PathVariable("taskId") Long taskId) {

        Result<HistoryDTO> result = historyService.getHistoryByTaskId(taskId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增审批记录", notes = "新增审批记录", produces = "application/json")
    @PostMapping("/addHistory/{applyId}")
    public JsonResult addHistory(@Valid @RequestBody HistoryVO historyVO) {

        BestBpmAsset.isAssetEmpty(historyVO);
        HistorySaveDTO historySaveDTO = switchToDTO(historyVO,HistorySaveDTO.class);
        historySaveDTO.setApproveUserId(currentUserInfo().getUserId());
        historySaveDTO.setApproveRealName(currentUserInfo().getRealName());
        historySaveDTO.setOperatorId(currentUserInfo().getUserId());
        historySaveDTO.setOperatorName(currentUserInfo().getRealName());
        Result<Integer> result = historyService.addHistory(historySaveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }
}

