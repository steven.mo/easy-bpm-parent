package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.MessageWhiteListQueryDTO;
import com.pig.easy.bpm.dto.request.MessageWhiteListSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MessageWhiteListDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.MessageWhiteListService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.MessageWhiteListQueryVO;
import com.pig.easy.bpm.vo.request.MessageWhiteListSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * <p>
 * 通知白名单 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
@RestController
@Api(tags = "通知白名单管理", value = "通知白名单管理")
@RequestMapping("/messageWhiteList")
public class MessageWhiteListController extends BaseController {

        @Reference
        MessageWhiteListService service;

        @ApiOperation(value = "查询通知白名单列表", notes = "查询通知白名单列表", produces = "application/json")
        @PostMapping("/getList")
        public JsonResult getList(@Valid @RequestBody MessageWhiteListQueryVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MessageWhiteListQueryDTO queryDTO = switchToDTO(param, MessageWhiteListQueryDTO.class);

        Result<PageInfo<MessageWhiteListDTO>> result = service.getListByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "新增通知白名单", notes = "新增通知白名单", produces = "application/json")
        @PostMapping("/insert")
        public JsonResult insertMessageWhiteList(@Valid @RequestBody MessageWhiteListSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
       MessageWhiteListSaveOrUpdateDTO saveDTO = switchToDTO(param, MessageWhiteListSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId().intValue());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.insertMessageWhiteList(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "修改通知白名单", notes = "修改通知白名单", produces = "application/json")
        @PostMapping("/update")
        public JsonResult updateMessageWhiteList(@Valid @RequestBody MessageWhiteListSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
         MessageWhiteListSaveOrUpdateDTO saveDTO = switchToDTO(param, MessageWhiteListSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId().intValue());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.updateMessageWhiteList(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "删除通知白名单", notes = "删除通知白名单", produces = "application/json")
        @PostMapping("/delete")
        public JsonResult delete(@Valid @RequestBody MessageWhiteListSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MessageWhiteListSaveOrUpdateDTO saveDTO = switchToDTO(param, MessageWhiteListSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId().intValue());
        saveDTO.setOperatorName(currentUserInfo().getRealName());
        saveDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = service.deleteMessageWhiteList(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

}

