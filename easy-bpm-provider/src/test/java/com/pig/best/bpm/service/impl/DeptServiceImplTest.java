package com.pig.easy.bpm.service.impl;

import com.pig.easy.bpm.dto.response.MenuTreeDTO;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.service.DeptService;
import com.pig.easy.bpm.utils.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

@SpringBootTest
public class DeptServiceImplTest {

    @Autowired
    DeptService deptService;

    @Test
    void getOrganTree() {

        Result<List<TreeDTO>> result = deptService.getOrganTree("pig", null);

        System.out.println("result = " + result);
        Assert.isTrue(result.getData().size() > 0 ,"");
    }

    @Test
    void getDeptTree() {

        Result<List<TreeDTO>> result = deptService.getDeptTree("pig");

        System.out.println("result = " + result);
        Assert.isTrue(result.getData().size() > 0 ,"");
    }
}