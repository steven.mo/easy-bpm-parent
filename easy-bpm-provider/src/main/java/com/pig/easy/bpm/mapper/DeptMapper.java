package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.request.DeptQueryDTO;
import com.pig.easy.bpm.dto.response.DeptDTO;
import com.pig.easy.bpm.entity.DeptDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
@Mapper
public interface DeptMapper extends BaseMapper<DeptDO> {

    List<DeptDTO> getListByCondition(DeptQueryDTO deptQueryDTO);
}
