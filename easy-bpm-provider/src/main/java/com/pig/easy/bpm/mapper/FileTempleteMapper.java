package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.entity.FileTempleteDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;

import java.util.List;

/**
 * <p>
 * 模板文件表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-08-10
 */
@Mapper
public interface FileTempleteMapper extends BaseMapper<FileTempleteDO> {

    List<FileTempleteDTO> getListByCondition(FileTempleteQueryDTO param);
}
