package com.pig.easy.bpm.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author pig
 * @since 2020-05-14
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("bpm_user")
public class UserDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;
    /**
     * 用户编号
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 用户账户
     */
    @TableField("user_name")
    private String userName;
    /**
     * 用户名称
     */
    @TableField("real_name")
    private String realName;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 联系方式
     */
    private String phone;

    /**
     * 头像链接
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 性别，0表示未知，1表示男，2表示女
     */
    private Integer gender;
    /**
     * 生日
     */
    @TableField("birth_date")
    private Date birthDate;
    /**
     * 公司编号
     */
    @TableField("company_id")
    private Long companyId;
    /**
     * 部门编号
     */
    @TableField("dept_id")
    private Long deptId;
    /**
     * 当前用户活动租户编号
     */
    @TableField("tenant_id")
    private String tenantId;

    /**
     * 密码
     */
    private String password;
    /**
     * 入职时间
     */
    @TableField("entry_time")
    private LocalDateTime entryTime;
    /**
     * 离职时间
     */
    @TableField("leave_time")
    private LocalDateTime leaveTime;
    /**
     * 雇佣状态 1 在职 2 离职
     */
    @TableField("hire_status")
    private Integer hireStatus;
    /**
     * 有效状态；0表示无效，1表示有效
     */
    @TableField("valid_state")
    private Integer validState;
    /**
     * 操作人工号
     */
    @TableField("operator_id")
    private Long operatorId;
    /**
     * 操作人姓名
     */
    @TableField("operator_name")
    private String operatorName;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    @TableField("position_code")
    private String positionCode;

    @TableField("position_name")
    private String positionName;


}
