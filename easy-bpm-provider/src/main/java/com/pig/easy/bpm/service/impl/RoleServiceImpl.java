package com.pig.easy.bpm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.RoleAddOrUpdateDTO;
import com.pig.easy.bpm.dto.request.RoleGroupRoleDetailQueryDTO;
import com.pig.easy.bpm.dto.request.RoleReqDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.RoleDTO;
import com.pig.easy.bpm.dto.response.RoleGroupRoleDetailDTO;
import com.pig.easy.bpm.entity.RoleDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.RoleMapper;
import com.pig.easy.bpm.service.RoleService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@Service
public class RoleServiceImpl extends BeseServiceImpl<RoleMapper, RoleDO> implements RoleService {

    @Autowired
    RoleMapper roleMapper;

    @Override
    public Result<PageInfo<RoleDTO>> getListByCondiction(RoleReqDTO roleReqDTO) {

        if (roleReqDTO == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        int pageIndex = CommonUtils.evalInt(roleReqDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(roleReqDTO.getPageSize(), DEFAULT_PAGE_SIZE);

        RoleDO roleDO = BeanUtils.switchToDO(roleReqDTO, RoleDO.class);
        roleDO.setValidState(VALID_STATE);
        PageHelper.startPage(pageIndex, pageSize);
        List<RoleDTO> roleDTOS = roleMapper.getListByCondiction(roleDO);
        if (roleDTOS == null) {
            roleDTOS = new ArrayList<>();
        }
        PageInfo<RoleDTO> pageInfo = new PageInfo<>(roleDTOS);
        return Result.responseSuccess(pageInfo);
    }

    @Override
    public Result<List<RoleDTO>> getList(RoleReqDTO roleReqDTO) {

        int pageIndex = CommonUtils.evalInt(roleReqDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(roleReqDTO.getPageSize(), MAX_PAGE_SIZE);
        roleReqDTO.setPageIndex(pageIndex);
        roleReqDTO.setPageSize(pageSize);
        Result<PageInfo<RoleDTO>> result = getListByCondiction(roleReqDTO);
        if(result.getEntityError().getCode() != EntityError.SUCCESS.getCode()){
            return Result.responseError(result.getEntityError());
        }
        return Result.responseSuccess(result.getData().getList());
    }

    @Override
    public Result<PageInfo<RoleGroupRoleDetailDTO>> getRoleGroupRoleDetailByCondition(RoleGroupRoleDetailQueryDTO queryDTO) {

        if(queryDTO == null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        int pageIndex = CommonUtils.evalInt(queryDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(queryDTO.getPageSize(), DEFAULT_PAGE_SIZE);
        queryDTO.setPageIndex(pageIndex);
        queryDTO.setPageSize(pageSize);
        List<RoleGroupRoleDetailDTO> result = new ArrayList<>();
        Integer count = roleMapper.getCountByCondition(queryDTO);
        if(count > 0){
            result =  roleMapper.getRoleGroupRoleDetailByCondition(queryDTO);
        }
        PageInfo<RoleGroupRoleDetailDTO> pageInfo = new PageInfo<>(result);
        pageInfo.setSize(count);
        return Result.responseSuccess(pageInfo);
    }

    @Override
    public Result<List<RoleGroupRoleDetailDTO>> getRoleGroupRoleDetailList(RoleGroupRoleDetailQueryDTO queryDTO){

        int pageIndex = CommonUtils.evalInt(queryDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(queryDTO.getPageSize(), MAX_PAGE_SIZE);
        queryDTO.setPageIndex(pageIndex);
        queryDTO.setPageSize(pageSize);
        Result<PageInfo<RoleGroupRoleDetailDTO>> result = getRoleGroupRoleDetailByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result.getEntityError());
        }
        return Result.responseSuccess(result.getData().getList());
    }

    @Override
    public Result<RoleDTO> getRole(RoleReqDTO roleReqDTO) {

        if (roleReqDTO == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        RoleDO roleDO = BeanUtils.switchToDO(roleReqDTO, RoleDO.class);
        roleDO.setValidState(VALID_STATE);
        roleDO = roleMapper.selectOne(new QueryWrapper<>(roleDO));
        if (roleDO == null) {
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }
        RoleDTO roleDTO = BeanUtils.switchToDO(roleDO, RoleDTO.class);
        return Result.responseSuccess(roleDTO);
    }

    @Override
    public Result<RoleDTO> getRoleByRoleCode(String roleCode) {

        if (StringUtils.isEmpty(roleCode)) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        RoleReqDTO reqDTO = new RoleReqDTO();
        reqDTO.setRoleCode(roleCode);
        Result<RoleDTO> result = getRole(reqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result.getEntityError());
        }
        return Result.responseSuccess(result.getData());
    }

    @Override
    public Result<RoleDTO> getRoleByRoleId(Long roleId) {
        if (CommonUtils.evalLong(roleId) < 0) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        RoleReqDTO reqDTO = new RoleReqDTO();
        reqDTO.setRoleId(roleId);
        Result<RoleDTO> result = getRole(reqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result.getEntityError());
        }
        return Result.responseSuccess(result.getData());
    }

    @Override
    public Result<Integer> insertRole(RoleAddOrUpdateDTO roleAddOrUpdateDTO) {

        if (roleAddOrUpdateDTO == null
                || StringUtils.isEmpty(roleAddOrUpdateDTO.getTenantId())
                || CommonUtils.evalLong(roleAddOrUpdateDTO.getDeptId()) < 0
                || CommonUtils.evalLong(roleAddOrUpdateDTO.getCompanyId()) < 0
                || StringUtils.isEmpty(roleAddOrUpdateDTO.getRoleCode())) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        // 默认 key 格式   租户 +":" + key
        if (!roleAddOrUpdateDTO.getRoleCode().startsWith((roleAddOrUpdateDTO.getTenantId()))) {
            roleAddOrUpdateDTO.setRoleCode(roleAddOrUpdateDTO.getTenantId()
                    + BpmConstant.COMMON_CODE_SEPARATION_CHARACTER
                    + "role"
                    + BpmConstant.COMMON_CODE_SEPARATION_CHARACTER
                    + roleAddOrUpdateDTO.getRoleCode());
        }

        RoleDO roleDO = BeanUtils.switchToDO(roleAddOrUpdateDTO, RoleDO.class);
        return Result.responseSuccess(roleMapper.insert(roleDO));
    }

    @Override
    public Result<Integer> updateRoleByRoleCode(RoleAddOrUpdateDTO roleAddOrUpdateDTO) {

        if (roleAddOrUpdateDTO == null
                || StringUtils.isEmpty(roleAddOrUpdateDTO.getRoleCode())) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        RoleDO roleDO = BeanUtils.switchToDO(roleAddOrUpdateDTO, RoleDO.class);
        Integer num = roleMapper.updateByRoleCode(roleDO);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<List<ItemDTO>> getRoleDictByTenantId(String tenantId) {

        BestBpmAsset.isAssetEmptyByDefault(tenantId);

        RoleReqDTO reqDTO = new RoleReqDTO();
        reqDTO.setTenantId(tenantId);
        Result<List<RoleDTO>> list = getList(reqDTO);
        if (list.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(list.getEntityError());
        }
        List<ItemDTO> result = new ArrayList<>();
        ItemDTO itemDTO = null;
        for(RoleDTO roleDTO : list.getData()){
            itemDTO = new ItemDTO();
            itemDTO.setValue(roleDTO.getRoleCode());
            itemDTO.setLabel(roleDTO.getRoleName());
            result.add(itemDTO);
        }
        itemDTO = null;
        return Result.responseSuccess(result);
    }
}
