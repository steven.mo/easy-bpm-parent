package com.pig.easy.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色组
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("bpm_role_group")
public class RoleGroupDO implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "role_group_id", type = IdType.AUTO)
    private Long roleGroupId;

    /**
     * 角色组编码
     */
    private String roleGroupCode;

    /**
     * 角色组名称
     */
    private String roleGroupName;

    /**
     * 角色组简称
     */
    private String roleGroupAbbr;

    /**
     * 角色组等级
     */
    private Integer roleGroupLevel;

    /**
     * 角色组类别
     */
    private Integer roleGroupType;

    /**
     * 所属条线
     */
    private String businessLine;

    /**
     * 租户编号
     */
    private String tenantId;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 状态 1 有效 0 失效
     */
    private Integer validState;

    /**
     * 操作人工号
     */
    private Long operatorId;

    /**
     * 操作人姓名
     */
    private String operatorName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
