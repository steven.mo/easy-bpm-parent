package com.pig.easy.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author pig
 * @since 2020-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("bpm_config_template")
public class ConfigTemplateDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 模板编号
     */
    @TableId(value = "template_id", type = IdType.AUTO)
    private Long templateId;
    /**
     * 模板编号
     */
    @TableField("template_code")
    private String templateCode;
    /**
     * 模板名称
     */
    @TableField("template_name")
    private String templateName;
    /**
     * 模板key
     */
    @TableField("template_key")
    private String templateKey;
    /**
     * 模板值
     */
    @TableField("template_value")
    private String templateValue;
    /**
     * 模板类型
     */
    @TableField("template_type")
    private String templateType;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 状态 1 有效 0 失效
     */
    @TableField("valid_state")
    private Integer validState;
    /**
     * 操作人工号
     */
    @TableField("operator_id")
    private Long operatorId;
    /**
     * 操作人姓名
     */
    @TableField("operator_name")
    private String operatorName;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    @TableField("template_status")
    private Integer templateStatus;


}
