package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.entity.VariableDictDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;

import java.util.List;

/**
 * <p>
 * 变量表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
@Mapper
public interface VariableDictMapper extends BaseMapper<VariableDictDO> {

    List<VariableDictDTO> getListByCondition(VariableDictQueryDTO param);
}
