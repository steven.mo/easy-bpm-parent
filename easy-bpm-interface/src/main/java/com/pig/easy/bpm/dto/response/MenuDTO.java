package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)


public class MenuDTO extends BaseResponseDTO implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    private Long menuId;

    /**
     * 资源编码
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单编码
     */
    private String menuIcon;


    /**
     * 租户编号
     */
    private String tenantId;
    /**
     * 菜单URL
     */
    private String menuUrl;

    /**
     * 菜单类型 menu: 菜单
     */
    private String menuType;

    /**
     * 上级编号 0为 1级
     */
    private Long parentId;


    /**
     * 组件地址
     */
    private String component;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 是否隐藏 0 不隐藏 1 隐藏
     */
    private Integer hidden;

    /**
     * 操作者工号
     */
    private Long operatorId;

    /**
     * 操作人名称
     */
    private String operatorName;

    private String meta;


    private Integer alwaysShow;


    private String redirect;


}
