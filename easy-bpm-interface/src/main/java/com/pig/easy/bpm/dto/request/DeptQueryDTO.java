package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/27 14:21
 */
@Data
@ToString
public class DeptQueryDTO extends BaseRequestDTO {

    private static final long serialVersionUID = -7587684899767489954L;

    private String parentDeptCode;

    private Long deptId;

    private String deptCode;

    private String deptName;

    private Long companyId;

    private String companyCode;

    private String tenantId;

    private Long deptParentId;

    private String deptParentCode;

    private Integer deptLevel;

    private String deptType;

    private String deptTypeCode;

    private String remark;

    private Integer deptOrder;

    private Integer validState;

    private String businessLine;

    private int pageIndex;

    private int pageSize;
}
